database = {} #um dicionário, que tem a chave interesses para o controle
#dos interesses (que pessoa se interessa por que outra), e pessoas para o controle de pessoas (quem sao as pessoas e quais sao os dados pessoais de cada pessoa no sistema)
#voce pode controlar as pessoas de outra forma se quiser, nao precisa mudar nada
#do seu código para usar essa váriavel
database['interesses'] = { 
    100: [101, 102, 103],
    200: [100]
}
database['PESSOA'] = [] #esse voce só faz se quiser guardar nessa lista os dicionários das pessoas

#em todo esse codigo que estou compartilhando, as variaveis interessado, alvo de interesse, pessoa, pessoa1 e pessoa2 sao sempre IDs de pessoas

class NotFoundError(Exception):
    pass

class IncompatibleError(Exception):
    pass

##Funções extras
def deletar_pessoa(id_pessoa):
    database['PESSOA'].remove(dict(localiza_pessoa(id_pessoa)))
##

def todas_as_pessoas():
    return database['PESSOA']

def adiciona_pessoa(dic_pessoa):
        database['PESSOA'].append(dic_pessoa)
        database['interesses'][dic_pessoa['id']]=[]

def localiza_pessoa(id_pessoa):
    for pessoa in database['PESSOA']:
        if id_pessoa == pessoa['id']: return pessoa
    raise NotFoundError


def reseta():
    database['interesses']={}
    database['PESSOA']=[]

def adiciona_interesse(id_interessado, id_alvo_de_interesse):          #TODO remover if inicial pois sempre haverá teste de compatibildiade 
    p1 = localiza_pessoa(id_interessado)
    p2 = localiza_pessoa(id_alvo_de_interesse)
    if 'sexo' in p1.keys():
        if p2['sexo'] in p1['buscando'] :
            database['interesses'][id_interessado].append(id_alvo_de_interesse)
        else: raise IncompatibleError
    else: 
        database['interesses'][id_interessado].append(id_alvo_de_interesse)

def consulta_interesses(id_interessado):
    return database['interesses'][localiza_pessoa(id_interessado)['id']]


def remove_interesse(id_interessado,id_alvo_de_interesse):
    localiza_pessoa(id_interessado)
    database['interesses'][id_interessado].remove(id_alvo_de_interesse)

#essa funcao diz se o 1 e o 2 tem match. (retorna True se eles tem, False se não)
#ela não está testada, só existe para fazer aquecimento para a próxima
def verifica_match(id1,id2):
    localiza_pessoa(id1)
    localiza_pessoa(id2)
    return (id2 in database['interesses'][id1] and id1 in database['interesses'][id2])
    

      
def lista_matches(id_pessoa):
    matches=[]
    localiza_pessoa(id_pessoa)
    for id_interesses in consulta_interesses(id_pessoa):
        if id_pessoa in consulta_interesses(id_interesses): matches.append(id_interesses)
    return matches