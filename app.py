from flask import Flask , jsonify, request
from backend.estrutura_interesses import *
import json


app = Flask(__name__)

@app.route("/")
def index():
    return jsonify(database)


##PESSOAS

@app.route("/users")
def users_list():
    return jsonify(todas_as_pessoas())

@app.route("/users/<id_user>",)
def user_dict(id_user):

    try:
        return jsonify(localiza_pessoa(int(id_user)))
    
    except NotFoundError:
        return "Pessoa não cadastrada", 404
    
@app.route("/users/new", methods = ["POST"])
def new_user():
        data = request.get_json()

        adiciona_pessoa(data)
        return "Adicionado com sucesso"


##INTERESSES

@app.route("/likes/<id_user>")
def likes_list(id_user):
    try:
        return jsonify(consulta_interesses(int(id_user)))
    
    except NotFoundError:
        return "Pessoa não cadastrada", 404
    
@app.route("/likes/new", methods=["POST"])
def new_like():
    data = request.get_json()

    try:
        adiciona_interesse(data['user'],data['liked'])
        return "Like adicionado com sucesso"
    
    except NotFoundError: return "Pessoa(as) não cadastrada(as)", 404
    except IncompatibleError : return "Icompatibilidade de preferência", 400

@app.route("/likes/delete", methods=["DELETE"])
def delete_like(): 
    data = request.get_json()

    try:
        remove_interesse(data["user"],data["liked"])
        return "Like removido com sucesso"
    
    except NotFoundError: return "Pessoa(as) não cadastrada(as)", 404

## MATCHES

@app.route("/matches/<id_user>")
def matches_list(id_user):
    try:
        return jsonify(lista_matches(int(id_user)))
    
    except NotFoundError: return "Pessoa não cadastrada", 404

#EXTRAS
@app.route("/users/del/<id_user>", methods= ["DELETE"])
def delete_user(id_user):
    try:
        deletar_pessoa(int(id_user))
        return "Pessoa deletada com sucesso"
    
    except NotFoundError:
        return "Pessoa não cadastrada", 404




if __name__=="__main__":
    app.run(debug=True, port=5001)